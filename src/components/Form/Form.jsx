import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import './Form.css';

const Form = ({createAppointement}) => {
    const [appointment, updateAppointment] = useState({
        pet: '',
        owner: '',
        date: '',
        time: '',
        symptoms: ''
    });

    const [error, updateError] = useState(false);

    const handleChange = (e) => {
        updateAppointment({
            ...appointment,
            [e.target.name]: e.target.value
        })
    }

    // Descoponetizar para no tener que usar appointment.pet y usar directamente pet
    const { pet, owner, date, time, symptoms} = appointment;

    const submitAppointment = (e) => {
        e.preventDefault();
        
        // Validar
        if(pet.trim() === '' || owner.trim() === '' || date.trim() === '' || time.trim() === '' || symptoms.trim() === ''){
            updateError(true);
            return;
        }

        // Eliminar el mensaje de error
        updateError(false);

        // Agregar ID
        appointment.id = uuidv4();

        // Crear cita
        createAppointement(appointment);

        // Vaciar los campos
        updateAppointment({
            pet: '',
            owner: '',
            date: '',
            time: '',
            symptoms: ''
        });
    }

    return (
        <>
            <h2>Crear cita</h2>

            { error ? <p className="alerta-error">Todos los campos son obligatorios</p> : null}

            <form onSubmit={submitAppointment}>
                <label>Nombre Mascota</label>
                <input
                    type="text"
                    name="pet"
                    className="u-full-width"
                    placeholder="Nombre Mascota"
                    onChange={handleChange}
                    value={pet}
                />

                <label>Nombre Dueño</label>
                <input
                    type="text"
                    name="owner"
                    className="u-full-width"
                    placeholder="Nombre dueño de la mascota"
                    onChange={handleChange}
                    value={owner}
                />

                <label>Fecha</label>
                <input
                    type="date"
                    name="date"
                    className="u-full-width"
                    onChange={handleChange}
                    value={date}
                />

                <label>Hora</label>
                <input
                    type="time"
                    name="time"
                    className="u-full-width"
                    onChange={handleChange}
                    value={time}
                />

                <label>Sintomas</label>
                <textarea
                    className="u-full-width"
                    name="symptoms"
                    onChange={handleChange}
                    value={symptoms}
                >
                </textarea>

                <button
                    type="submit"
                    className="u-full-width button-primary"
                    onChange={handleChange}
                >Agregar cita</button>
            </form>
        </>
    );
}

export default Form;