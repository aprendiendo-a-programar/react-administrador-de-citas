import './Appointment.css';

const Appointment = ({appointment, deleteAppointment}) => {
    return (
        <div className="appointment">
            <p className="appointment__title">Mascota: <span className="appointment__info">{appointment.pet}</span></p>
            <p className="appointment__title">Dueño: <span className="appointment__info">{appointment.owner}</span></p>
            <p className="appointment__title">Fecha: <span className="appointment__info">{appointment.date}</span></p>
            <p className="appointment__title">Hora: <span className="appointment__info">{appointment.time}</span></p>
            <p className="appointment__title">Síntomas: <span className="appointment__info">{appointment.symptoms}</span></p>

            <button
                className="button u-full-width"
                onClick={() => deleteAppointment(appointment.id)}
            >Eliminar &times;</button>
        </div>
    );
}
 
export default Appointment;