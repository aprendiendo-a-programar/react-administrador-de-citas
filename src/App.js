import { useState, useEffect } from 'react';
import Form from './components/Form/Form.jsx';
import Appointment from './components/Appointment/Appointment';
import './App.css';

function App() {
  // Citas local storage
  let initialAppointments = JSON.parse(localStorage.getItem('appointments'));
  if(!initialAppointments) {
    initialAppointments = [];
  }

  // arreglo de citas
  const [appointments, saveAppointment] = useState(initialAppointments);

  // Use Effect para realizar las operaciones cuando el state cambia
  useEffect(() => {
    if(initialAppointments) {
      localStorage.setItem('appointments', JSON.stringify(appointments));
    } else {
      localStorage.setItem('appointments', JSON.stringify([]));
    }
  }, [appointments, initialAppointments]);

  // Función que tome las citas actuales y añada las nuevas
  const createAppointement = (newAppointment) => {
    saveAppointment([
      ...appointments,
      newAppointment
    ])
  }

  // Función que elimina una cita por su id
  const deleteAppointment = id => {
    const newAppointment = appointments.filter(appointment => appointment.id !== id);
    saveAppointment(newAppointment);
  }

  // Mensaje condicional del carrito
  const title = appointments.length === 0 ? 'No hay citas' : 'Administra tus citas';

  return (
    <>
      <h1>Administrador de pacientes</h1>

      <div className="container">
        <div className="row">

          <div className="one-half column">
            <Form
              createAppointement={createAppointement}
            />
          </div>

          <div className="one-half column">
            <h2>{title}</h2>

            {appointments.map(appointment => (
              <Appointment
                key={appointment.id}
                appointment={appointment}
                deleteAppointment={deleteAppointment}
              />
            ))}
          </div>

        </div>
      </div>
    </>
  );
}

export default App;
